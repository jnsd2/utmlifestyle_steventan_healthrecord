package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.utmlifestyle.Model.Health_Record;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class health_record_display extends AppCompatActivity {



    TextView ETReason, ETMedication, ETDortor_note, ETclinic_receive;
    Spinner spinner_date;
    Spinner spinner2;

    List<Health_Record> health_recordList = new ArrayList<>();
    FirebaseUser user;
    DatabaseReference reference;

    String userID;
    CircleImageView healthRecordImageView;
    String PROFILE_IMAGE_URL = null;
    int TAKE_IMAGE_CODE = 10001;

    int SELECT_FILE = 10011;
    Uri ImageUri;
    TextView ETname, ETid;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_record_display);

        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        userID = user.getUid();
        healthRecordImageView = findViewById(R.id.healthRecordImageView);

        spinner2 = findViewById(R.id.spinner_date);

        ETclinic_receive = findViewById(R.id.clinic_visits);
        final TextView clinic_visits = findViewById(R.id.clinic_visits);

        if (user.getPhotoUrl() != null) {
            Glide.with(this)
                    .load(user.getPhotoUrl())
                    .into(healthRecordImageView);
        }
        ETname = findViewById(R.id.textName);
        ETid = findViewById(R.id.textID);

        final TextView textName = findViewById(R.id.textName);
        final TextView textID = findViewById(R.id.textID);

        reference.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User userProfile = snapshot.getValue(User.class);
                String name = userProfile.name;
                id = userProfile.id;

                textName.setText(name);
                textID.setText(id);

                //user = FirebaseAuth.getInstance().getCurrentUser();
                reference = FirebaseDatabase.getInstance().getReference().child("Health_Record");
                userID = user.getUid();

                ETReason = findViewById(R.id.textReason);
                ETMedication = findViewById(R.id.textMedication);
                ETDortor_note = findViewById(R.id.textDoctorNote);

                final TextView textReason = findViewById(R.id.textReason);
                final TextView textMedication = findViewById(R.id.textMedication);
                final TextView textDortorNote = findViewById(R.id.textDoctorNote);
                //Toast.makeText(health_record_display.this, userID, Toast.LENGTH_LONG).show();
                reference.orderByChild("id").equalTo(id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        final List<String> date = new ArrayList<String>();
                        for(DataSnapshot snapshot1 : snapshot.getChildren()){
                            Health_Record record = snapshot1.getValue(Health_Record.class);
                            health_recordList.add(record);
                            String selectdate = snapshot1.child("date").getValue(String.class);
                            date.add(selectdate);
                            if(record.getId().equals(id)){
                                textReason.setText(record.getReason());
                                textMedication.setText(record.getMedication());
                                textDortorNote.setText(record.getNote());

                            }
                            //Toast.makeText(health_record_display.this, record.getId()+"::"+id, Toast.LENGTH_LONG).show();
                            //health_recordList.add(record);
                        }
                /*Health_Record record = snapshot.getValue(Health_Record.class);
                String reason = record.getReason();*/
                        ArrayAdapter<String> datesAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner1,date);
                        datesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner2.setAdapter(datesAdapter);
                        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id2) {
                                //Toast.makeText(health_record_display.this, "Click", Toast.LENGTH_SHORT).show();
                                String text2 = parent.getItemAtPosition(position).toString();
                                clinic_visits.setText(text2);
                                for(int i = 0; i <health_recordList.size() ; i++){
                                    //Toast.makeText(health_record_display.this, health_recordList.get(i).getDate()+"::"+text2+"\n"+health_recordList.get(i).getId()+"::"+id, Toast.LENGTH_SHORT).show();
                                    if(health_recordList.get(i).getDate().equals(text2)&&health_recordList.get(i).getId().equals(id)){
                                        textReason.setText(health_recordList.get(i).getReason());
                                        textMedication.setText(health_recordList.get(i).getMedication());
                                        textDortorNote.setText(health_recordList.get(i).getNote());

                                    }
                                }

                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(health_record_display.this, "Something wrong happened!", Toast.LENGTH_LONG).show();
                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(health_record_display.this, "Something wrong happened!", Toast.LENGTH_LONG).show();
            }
        });




    }
}