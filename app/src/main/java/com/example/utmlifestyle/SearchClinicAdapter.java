package com.example.utmlifestyle;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;

public class SearchClinicAdapter extends RecyclerView.Adapter<SearchClinicAdapter.ViewHolder> {

    Context context;
    List<ClinicSearch> clinicSearchList = new ArrayList<>();

    SearchClinicAdapter(Context context, List<ClinicSearch> clinicSearchList){
        this.context = context;
        this.clinicSearchList = clinicSearchList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_clinic_single2, parent, false);
        return new SearchClinicAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textview_apptID.setText(this.clinicSearchList.get(position).getApptID());
        holder.textview_date.setText(this.clinicSearchList.get(position).getDate());
        holder.textview_time.setText(this.clinicSearchList.get(position).getEtChooseTime());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();

                final String date = clinicSearchList.get(position).getDate();
                final String time  = clinicSearchList.get(position).getEtChooseTime();
                final String apptID  = clinicSearchList.get(position).getApptID();

                Intent intent = new Intent(context, clinic_manage.class);
                intent.putExtra("date", date);
                intent.putExtra("time", time);
                intent.putExtra("apptID", apptID);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.clinicSearchList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textview_date, textview_time, textview_apptID;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textview_date = itemView.findViewById(R.id.textDate);
            textview_time = itemView.findViewById(R.id.textTime);
            textview_apptID = itemView.findViewById(R.id.apptID);
            imageView = itemView.findViewById(R.id.imageView8);
        }
    }
}
