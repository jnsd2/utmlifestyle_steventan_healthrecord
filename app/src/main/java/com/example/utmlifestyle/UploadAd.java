package com.example.utmlifestyle;

public class UploadAd {
    private String type, title, date, time, description, url;

    public UploadAd(){

    }

    public UploadAd(String adType, String adTitle, String adDate, String adTime, String adDescription, String adURL){

        type = adType;
        title = adTitle;
        date = adDate;
        time = adTime;
        description = adDescription;
        url = adURL;
    }

    public String getType(){
        return type;
    }

    public void setType(String adType){
        type=adType;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String adTitle){
        title=adTitle;
    }

    public String getDate(){
        return date;
    }

    public void setDate(String adDate){
        date=adDate;
    }

    public String getTime(){
        return time;
    }

    public void setTime(String adTime){
        time=adTime;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String adDescription){
        description=adDescription;
    }

    public String getUrl(){
        return url;
    }

    public void setUrl(String adURL){
        url=adURL;
    }

}
