package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Pattern;

public class admin_addAD extends AppCompatActivity {

    EditText adType, adTitle, adDate, adTime,  adDescription;

    Button submitButton;
    DatabaseReference add_ad;

    Advertisement advertisement;

    private boolean ValidType(String type) {

        String TYPE_STRING = "SPORTS|HEALTH";

        return Pattern.compile(TYPE_STRING).matcher(type).matches();

    }

    private boolean ValidDate(String date) {

        String DATE_STRING = "^\\s*(3[01]|[12][0-9]|0?[1-9])\\/(1[012]|0?[1-9])\\/((?:19|20)\\d{2})\\s*$";

        return Pattern.compile(DATE_STRING).matcher(date).matches();
    }

    //"^(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((?:19|20)[0-9][0-9])$";

    private boolean ValidTime(String time) {

        String TIME_STRING = "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";

        return Pattern.compile(TIME_STRING).matcher(time).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_a_d);

        adType = findViewById(R.id.adType);
        adTitle = findViewById(R.id.adTitle);
        adDate = findViewById(R.id.adDate);
        adTime = findViewById(R.id.adTime);
        adDescription = findViewById(R.id.adDescription);

        submitButton = findViewById(R.id.submitButton);

        advertisement = new Advertisement();
        add_ad = FirebaseDatabase.getInstance().getReference().child("Advertisement");

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                advertisement.setType(adType.getText().toString());
                advertisement.setTitle(adTitle.getText().toString());
                advertisement.setDate(adDate.getText().toString());
                advertisement.setTime(adTime.getText().toString());
                advertisement.setDescription(adDescription.getText().toString());

                if (TextUtils.isEmpty(adType.getText())){
                    Toast.makeText(admin_addAD.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                    adType.setError( "Dont Leave Me Empty :(" );
                }else if(!ValidType(adType.getText().toString())){

                    adType.setError( "Invalid Category! \n please enter in CAPITAL LETTER" );

                }else if (TextUtils.isEmpty(adTitle.getText())){
                    Toast.makeText(admin_addAD.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                    adTitle.setError( "Dont Leave Me Empty :(" );
                }else if (TextUtils.isEmpty(adDate.getText())){
                    Toast.makeText(admin_addAD.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                    adDate.setError( "Dont Leave Me Empty :(" );
                }else if(!ValidDate(adDate.getText().toString())){

                    adDate.setError( "Invalid Date Or Wrong Format \n Please Use DD/MM/YYYY!" );

                }else if (TextUtils.isEmpty(adTime.getText())){
                    Toast.makeText(admin_addAD.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                    adTime.setError( "Dont Leave Me Empty :(" );
                }else if(!ValidTime(adTime.getText().toString())){

                    adTime.setError( "Invalid Time Or Wrong Format \n Please Specify HH:MM (24 Hour System)" );

                }else if (TextUtils.isEmpty(adDescription.getText())){
                    Toast.makeText(admin_addAD.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                    adDescription.setError( "Dont Leave Me Empty :(" );
                }else{
                    add_ad.push().setValue(advertisement);

                    Toast.makeText(admin_addAD.this, "Added!", Toast.LENGTH_LONG).show();
                }

            }
        });

    }
}