package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class clinic_cancel extends AppCompatActivity {

    TextView date, time, apptID;

    Button cancel, back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_cancel);

        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        apptID = findViewById(R.id.apptID);

        String adate = getIntent().getStringExtra("date");
        String atime = getIntent().getStringExtra("time");
        String aapptID = getIntent().getStringExtra("apptID");

        date.setText(adate);
        time.setText(atime);
        apptID.setText(aapptID);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}