package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class clinic_search extends AppCompatActivity {

    private RecyclerView recyclerView;
    private SearchClinicAdapter searchClinicAdapter;

    private FirebaseUser user;
    private DatabaseReference reference;
    private Query query;

    private String userID;

    private FirebaseRecyclerOptions<ClinicSearch> display;
    private FirebaseRecyclerAdapter<ClinicSearch,ClinicView> adapter;

    EditText userID2;
    String userIDt;

    TextView date;
    String gDate;
    List<ClinicSearch> clinicSearchList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_search);

        Intent i = getIntent();
        gDate = i.getStringExtra("date");
        date = findViewById(R.id.date);
        date.setText(gDate);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        user = FirebaseAuth.getInstance().getCurrentUser();

        userID = user.getUid();

        userID2 = findViewById(R.id.userID2);
        reference = FirebaseDatabase.getInstance().getReference("Clinic");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot snapshot1: snapshot.getChildren()){
                    ClinicSearch clinicSearch = snapshot1.getValue(ClinicSearch.class);
                    //Toast.makeText(clinic_search.this,clinicSearch.getDate(), Toast.LENGTH_SHORT).show();
                    if(clinicSearch.getDate().equals(gDate)){
                        clinicSearchList.add(clinicSearch);
                   }

                }
                //
                searchClinicAdapter = new SearchClinicAdapter(clinic_search.this, clinicSearchList);
                recyclerView.setAdapter(searchClinicAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}