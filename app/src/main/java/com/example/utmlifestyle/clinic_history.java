package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class clinic_history extends AppCompatActivity {

    private RecyclerView recyclerView;

    private FirebaseUser user;
    private DatabaseReference reference;
    private Query query;

    private String userID;

    private FirebaseRecyclerOptions<ClinicSearch> display;
    private FirebaseRecyclerAdapter<ClinicSearch,ClinicView> adapter;

    EditText userID2;
    String userIDt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_history);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        userID = user.getUid();

        userID2 = findViewById(R.id.userID2);

        reference.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);

                if (user != null){
                    String idd = user.id;
                    userID2.setText(idd);

                    userIDt = userID2.getText().toString();

                    //Toast.makeText(clinic_history.this, userIDt, Toast.LENGTH_LONG).show();

                    query = FirebaseDatabase.getInstance().getReference().child("Clinic").orderByChild("id").equalTo(userIDt);

                    display = new FirebaseRecyclerOptions.Builder<ClinicSearch>().setQuery(query, ClinicSearch.class).build();
                    adapter = new FirebaseRecyclerAdapter<ClinicSearch, ClinicView>(display) {
                        @Override
                        protected void onBindViewHolder(@NonNull ClinicView holder, int position, @NonNull ClinicSearch model) {



                            final String date = model.getDate();
                            final String time = model.getEtChooseTime();
                            final String reason = model.getReason();
                            final String id = model.getId();
                            final String apptID = model.getApptID();

                            holder.view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), clinic_manage.class);
                                    intent.putExtra("date", date);
                                    intent.putExtra("time", time);
                                    intent.putExtra("reason", reason);
                                    intent.putExtra("id", id);
                                    intent.putExtra("apptID", apptID);
                                    startActivity(intent);
                                }
                            });
                            
                            holder.textDate.setText(""+model.getDate());
                            holder.textTime.setText(""+model.getEtChooseTime());
                            holder.apptID.setText(""+model.getApptID());

                        }

                        @NonNull
                        @Override
                        public ClinicView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_clinic_single2, parent, false);
                            return new ClinicView(v);
                        }
                    };

                    adapter.startListening();
                    recyclerView.setAdapter(adapter);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(clinic_history.this, "Connection to database failed :(", Toast.LENGTH_LONG).show();
            }
        });
        //end of recycle view
    }
}