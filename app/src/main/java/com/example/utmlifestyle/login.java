package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class login extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;

    private TextView forgotPassword, textRegister;

    private Button login;
    private EditText ETemail, ETpassword;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        login = findViewById(R.id.login);
        login.setOnClickListener(this);

        ETemail = findViewById(R.id.email);
        ETpassword = findViewById(R.id.password);

        progressBar = findViewById(R.id.progressBar);

        forgotPassword = findViewById(R.id.forgotPassword);
        forgotPassword.setOnClickListener(this);

        textRegister = findViewById(R.id.textRegister);
        textRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login:
                userLogin();
                break;

            case R.id.forgotPassword:
                startActivity(new Intent(login.this, forgotPassword.class));
                break;

            case R.id.textRegister:
                startActivity(new Intent(login.this, register.class));
                finish();
                break;
        }

    }

    private void userLogin() {

        String email = ETemail.getText().toString().trim();
        String password = ETpassword.getText().toString().trim();

        if (email.isEmpty()){
            ETemail.setError("Required!");
            ETemail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            ETemail.setError("Invalid Email!");
            ETemail.requestFocus();
            return;
        }

        if (password.isEmpty()){
            ETpassword.setError("Required!");
            ETpassword.requestFocus();
            return;
        }

        if (password.length() < 6){
            ETpassword.setError("Password should be more than 6 characters");
        }

        progressBar.setVisibility(View.VISIBLE);

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    if (user.isEmailVerified()){

                        Toast.makeText(login.this, "success", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                        startActivity(new Intent(login.this, home.class));
                        finish();
                    } else {
                        user.sendEmailVerification();
                        progressBar.setVisibility(View.GONE);
                        startActivity(new Intent(login.this, login2.class));
                    }


                }else {
                    Toast.makeText(login.this, "Login Failed. Please check your credentials", Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

    }
}