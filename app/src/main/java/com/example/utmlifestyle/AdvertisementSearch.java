package com.example.utmlifestyle;

public class AdvertisementSearch {
    private String date, description, time, title, type, url;


    public AdvertisementSearch() {
    }

    public AdvertisementSearch(String date, String description, String time, String title, String type, String url) {
        this.date = date;
        this.description = description;
        this.time = time;
        this.title = title;
        this.type = type;
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
