package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Pattern;

public class clinic_appointment extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    EditText edittext;

    EditText chooseTime;
    TimePickerDialog timePickerDialog;
    Calendar calendar;
    int currentHour;
    int currentMinute;

    EditText reason;

    EditText mid, mdate, mtime, mreason;

    DatabaseReference referencec;
    Clinic clinic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_appointment);

        reason = findViewById(R.id.reason);

        mid = findViewById(R.id.id);
        mdate = findViewById(R.id.date);
        mtime = findViewById(R.id.etChooseTime);
        mreason = findViewById(R.id.reason);


        //get id number
        String userID;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userID = user.getUid();
        DatabaseReference reference2 = FirebaseDatabase.getInstance().getReference("Users");
        final TextView id = findViewById(R.id.id);
        reference2.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User userProfile = snapshot.getValue(User.class);

                if (userProfile != null){
                    String ids = userProfile.id;

                    id.setText(ids);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

                Toast.makeText(clinic_appointment.this, "Something wrong happened!", Toast.LENGTH_LONG).show();

            }
        });

        //date picker
        final Calendar myCalendar = Calendar.getInstance();
        edittext = (EditText) findViewById(R.id.date);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                edittext.setText(sdf.format(myCalendar.getTime()));
                mdate.setText(sdf.format(myCalendar.getTime()));
            }
        };

        edittext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(clinic_appointment.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        //end of date picker

        //time picker
        chooseTime = findViewById(R.id.etChooseTime);
        chooseTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMinute = calendar.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(clinic_appointment.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {

                        chooseTime.setText(String.format("%02d:%02d", hourOfDay, minutes));
                        mtime.setText(String.format("%02d:%02d", hourOfDay, minutes));
                    }
                }, currentHour, currentMinute, false);

                timePickerDialog.show();
            }
        });
        //end of time picker

        //clinic reason spinner
        Spinner spinner = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.clinic, R.layout.custom_spinner1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        //end of clinic reason spinner

        //insert data in firebase
        referencec = FirebaseDatabase.getInstance().getReference().child("Clinic");
        clinic = new Clinic();
        Button submitButton = findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String kDate = mdate.getText().toString().trim();
                String kTime = mtime.getText().toString().trim();

                String kkID = "UTMHCADBT" + kDate + "TM" +kTime + "XYZ";


                clinic.setId(mid.getText().toString().trim());
                clinic.setReason(mreason.getText().toString().trim());
                clinic.setDate(edittext.getText().toString().trim());
                clinic.setEtChooseTime(chooseTime.getText().toString().trim());
                clinic.setApptID(kkID);


                if(TextUtils.isEmpty(mid.getText())){

                    Toast.makeText(clinic_appointment.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                }else if(TextUtils.isEmpty(mdate.getText())){

                    Toast.makeText(clinic_appointment.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                }else if(TextUtils.isEmpty(mtime.getText())){

                    Toast.makeText(clinic_appointment.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                }else if(TextUtils.isEmpty(mreason.getText())){

                    Toast.makeText(clinic_appointment.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                }else {
                    //referencec.push().setValue(clinic);
                    //Toast.makeText(clinic_appointment.this, "yay!", Toast.LENGTH_LONG).show();

                    /*referencec.child(kkID).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {

                            if (snapshot.exists()){
                                Toast.makeText(clinic_appointment.this, "The time slot is not available!", Toast.LENGTH_LONG).show();
                            }else {
                                referencec.child(kkID).push().setValue(clinic);

                                Intent i = new Intent(getApplicationContext(), clinic_confirm.class);
                                i.putExtra("AID", kkID);
                                i.putExtra("ADate", kDate);
                                i.putExtra("ATime", kTime);
                                startActivity(i);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            //empty
                        }
                    });*/

                    referencec.orderByChild("apptID").equalTo(kkID).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                Toast.makeText(clinic_appointment.this, "The time slot is booked!", Toast.LENGTH_LONG).show();
                            }else {
                                referencec.push().setValue(clinic);
                                Intent i = new Intent(getApplicationContext(), clinic_confirm.class);
                                i.putExtra("AID", kkID);
                                i.putExtra("ADate", kDate);
                                i.putExtra("ATime", kTime);
                                startActivity(i);
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            //empty
                        }
                    });
                }
            }
        });
        //end of insert data in firebase
    }

    //clinic reason spinner
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        reason.setText(text);
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    //empty
    }
    //end of clinic reason spinner
}