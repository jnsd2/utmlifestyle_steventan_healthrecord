package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class clinic_manage extends AppCompatActivity {

    TextView date, time, apptID;

    Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_manage);

        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        apptID = findViewById(R.id.apptID);

        String adate = getIntent().getStringExtra("date");
        String atime = getIntent().getStringExtra("time");
        String aapptID = getIntent().getStringExtra("apptID");

        date.setText(adate);
        time.setText(atime);
        apptID.setText(aapptID);


        //cancel booking
        cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), clinic_cancel.class);
                intent.putExtra("date", adate);
                intent.putExtra("time", atime);
                intent.putExtra("apptID", aapptID);
                startActivity(intent);
            }
        });

    }
}