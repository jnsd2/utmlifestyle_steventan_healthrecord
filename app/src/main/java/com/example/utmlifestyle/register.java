package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Pattern;

public class register extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;

    private Button register;
    private EditText ETname, ETid, ETdob, ETemail, ETphone, ETaddress, ETpassword, ETconfirmPassword;

    private ProgressBar progressBar;

    private TextView textLogin;

    private boolean ValidDate(String date) {

        String DATE_STRING = "^\\s*(3[01]|[12][0-9]|0?[1-9])\\/(1[012]|0?[1-9])\\/((?:19|20)\\d{2})\\s*$";

        return Pattern.compile(DATE_STRING).matcher(date).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        register = (Button) findViewById(R.id.register);
        register.setOnClickListener(this);

        textLogin = findViewById(R.id.textLogin);
        textLogin.setOnClickListener(this);

        ETname = findViewById(R.id.name);
        ETid = findViewById(R.id.id);
        ETdob = findViewById(R.id.dob);
        ETemail = findViewById(R.id.email);
        ETphone = findViewById(R.id.phone);
        ETaddress = findViewById(R.id.address);
        ETpassword = findViewById(R.id.password);
        ETconfirmPassword = findViewById(R.id.confirmPassword);

        progressBar = findViewById(R.id.progressBar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.register:
                registerUser();
                break;

            case R.id.textLogin:
                Intent i = new Intent(getApplicationContext(), login.class);
                startActivity(i);
                finish();
                break;
        }
    }

    private void registerUser() {

        String name = ETname.getText().toString().trim();
        String id = ETid.getText().toString().trim();
        String dob = ETdob.getText().toString().trim();
        String email = ETemail.getText().toString().trim();
        String phone = ETphone.getText().toString().trim();
        String address = ETaddress.getText().toString().trim();
        String password = ETpassword.getText().toString().trim();
        String confirmPassword = ETconfirmPassword.getText().toString().trim();

        if (name.isEmpty()){
            ETname.setError("Required!");
            ETname.requestFocus();
            return;
        }

        if (id.isEmpty()){
            ETid.setError("Required!");
            ETid.requestFocus();
            return;
        }

        if (dob.isEmpty()){
            ETdob.setError("Required!");
            ETdob.requestFocus();
            return;
        }

        if (!ValidDate(ETdob.getText().toString())){
            ETdob.setError("Invalid Date Or Wrong Format \n Please Use DD/MM/YYYY!");
            ETdob.requestFocus();
            return;
        }

        if (email.isEmpty()){
            ETemail.setError("Required!");
            ETemail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            ETemail.setError("Invalid Email!");
            ETemail.requestFocus();
            return;
        }

        if (phone.isEmpty()){
            ETphone.setError("Required!");
            ETphone.requestFocus();
            return;
        }

        if (!Patterns.PHONE.matcher(phone).matches()){
            ETphone.setError("Invalid Phone Number!");
            ETphone.requestFocus();
            return;
        }

        if (address.isEmpty()){
            ETaddress.setError("Required!");
            ETaddress.requestFocus();
            return;
        }

        if (password.isEmpty()){
            ETpassword.setError("Required!");
            ETpassword.requestFocus();
            return;
        }

        if (password.length() < 6){
            ETpassword.setError("Password should be more than 6 characters");
        }

        if (confirmPassword.isEmpty()){
            ETconfirmPassword.setError("Required!");
            ETconfirmPassword.requestFocus();
            return;
        }

        if (!confirmPassword.equals(password)){
            ETconfirmPassword.setError("Password do not match!");
            ETconfirmPassword.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()){
                            User user = new User(name, id, dob, phone, email, address);

                            FirebaseDatabase.getInstance().getReference("Users")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if (task.isComplete()){

                                        progressBar.setVisibility(View.GONE);
                                        Toast.makeText(register.this, "Register Successful", Toast.LENGTH_LONG).show();

                                        Intent i = new Intent(getApplicationContext(), register2.class);
                                        startActivity(i);
                                        finish();
                                    }else {

                                        progressBar.setVisibility(View.GONE);
                                        Toast.makeText(register.this, "An account with similar email has been created. Please try with another email.", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }else {

                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(register.this, "An account with similar email has been created. Please try with another email.", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}