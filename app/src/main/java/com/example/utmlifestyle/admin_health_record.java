package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class admin_health_record extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    HealthRecord hr;
    DatabaseReference referencehr;

    EditText mid, mdate, mreason, mmedication, mnote;

    EditText edittext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_health_record);

        mid = findViewById(R.id.id);
        mdate = findViewById(R.id.date);
        mreason = findViewById(R.id.reason);
        mmedication = findViewById(R.id.medication);
        mnote = findViewById(R.id.note);

        //date picker
        //date picker
        final Calendar myCalendar = Calendar.getInstance();
        edittext = (EditText) findViewById(R.id.date);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                edittext.setText(sdf.format(myCalendar.getTime()));
                mdate.setText(sdf.format(myCalendar.getTime()));
            }
        };

        edittext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(admin_health_record.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        //end of date picker

        //insert data in firebase
        referencehr = FirebaseDatabase.getInstance().getReference().child("Health_Record");
        hr = new HealthRecord();
        Button submitButton = findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hr.setId(mid.getText().toString().trim());
                hr.setDate(mdate.getText().toString().trim());
                hr.setReason(mreason.getText().toString().trim());
                hr.setMedication(mmedication.getText().toString().trim());
                hr.setNote(mnote.getText().toString().trim());


                if(TextUtils.isEmpty(mid.getText())){

                    Toast.makeText(admin_health_record.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                }else if(TextUtils.isEmpty(mdate.getText())){

                    Toast.makeText(admin_health_record.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                }else if(TextUtils.isEmpty(mreason.getText())){

                    Toast.makeText(admin_health_record.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                }else if(TextUtils.isEmpty(mmedication.getText())){

                    Toast.makeText(admin_health_record.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                }else if(TextUtils.isEmpty(mnote.getText())){

                    Toast.makeText(admin_health_record.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                }else {

                    referencehr.push().setValue(hr).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(admin_health_record.this, "Success", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        });
        //end of insert data in firebase

        //clinic reason spinner
        Spinner spinner = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.clinic, R.layout.custom_spinner1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        //end of clinic reason spinner
    }

    //clinic reason spinner
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        mreason.setText(text);
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //empty
    }
    //end of clinic reason spinner
}