package com.example.utmlifestyle;

public class ClinicSearch {

    private String date, etChooseTime, id, reason, apptID;

    public ClinicSearch() {

    }

    public ClinicSearch(String date, String etChooseTime, String id, String reason, String appID) {
        this.date = date;
        this.etChooseTime = etChooseTime;
        this.id = id;
        this.reason = reason;
        this.apptID = apptID;
    }

    public String getApptID() {
        return apptID;
    }

    public void setApptID(String apptID) {
        this.apptID = apptID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEtChooseTime() {
        return etChooseTime;
    }

    public void setEtChooseTime(String etChooseTime) {
        this.etChooseTime = etChooseTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
