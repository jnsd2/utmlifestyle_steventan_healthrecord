package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

public class home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Button toContactForm, toAboutApp, toAdvertisement, toProfile, logout, drawerButton, clinic, admin_h_r, health_record;

    private TextView ETname, ETemail;
    CircleImageView profileImageView;
    private String userID;
    private FirebaseUser user;
    private DatabaseReference reference;

    private DrawerLayout drawer_layout;
    private NavigationView nav_view;

    private FirebaseAuth mAuth;

    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private boolean validPhone(String phone) {
        Pattern pattern = Patterns.PHONE;
        return pattern.matcher(phone).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mAuth = FirebaseAuth.getInstance();

        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        userID = user.getUid();
        profileImageView = findViewById(R.id.profileImageView);

        toContactForm = findViewById(R.id.toContactForm);
        toAboutApp = findViewById(R.id.toAboutApp);
        toAdvertisement = findViewById(R.id.toAdvertisement);
        toProfile = findViewById(R.id.toProfile);
        logout = findViewById(R.id.logout);
        clinic = findViewById(R.id.clinic);
        admin_h_r = findViewById(R.id.admin_h_r);
        health_record = findViewById(R.id.health_record);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        drawerButton = findViewById(R.id.drawerButton);
        nav_view = findViewById(R.id.nav_view);
        drawer_layout = findViewById(R.id.drawer_layout);

        drawer_layout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                drawer_layout.setElevation(10f);
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                drawer_layout.setElevation(10f);
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                drawer_layout.setElevation(0f);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });

        drawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.openDrawer(GravityCompat.START);
                } else {
                    drawer_layout.closeDrawer(GravityCompat.END);
                }
            }
        });

        toAdvertisement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), advertisement_all.class);
                startActivity(i);
            }
        });

        toContactForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), contact_form.class);
                startActivity(i);
            }
        });

        toAboutApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), about_app.class);
                startActivity(i);
            }
        });

        toProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), profile.class);
                startActivity(i);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
                Toast.makeText(home.this, "You have been sign out.", Toast.LENGTH_LONG).show();
            }
        });

        clinic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), clinic_home.class);
                startActivity(i);
            }
        });

        admin_h_r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), admin_health_record.class);
                startActivity(i);
            }
        });

        health_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), health_record_display.class);
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_sbooking:
                Toast.makeText(home.this, "Venue Booking", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_srental:
                Toast.makeText(home.this, "Equipment Rental", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_sbmi:
                Toast.makeText(home.this, "BMI Calculator", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_srun:
                Toast.makeText(home.this, "Fitness Tracker", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_happointment:
                Toast.makeText(home.this, "Clinic Appointment", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_hrecord:
                Toast.makeText(home.this, "Health Record", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_notification:
                Toast.makeText(home.this, "Notification Settings", Toast.LENGTH_SHORT).show();
                break;
            case R.id.profile:
                startActivity(new Intent(home.this, profile.class));
                break;
            case R.id.nav_contact:
                startActivity(new Intent(home.this, contact_form.class));
                break;
            case R.id.nav_about:
                startActivity(new Intent(home.this, about_app.class));
                break;
        }
        drawer_layout.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void drawerButton(View view) {
    }
}