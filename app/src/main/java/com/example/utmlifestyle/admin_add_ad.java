package com.example.utmlifestyle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.OnProgressListener;
import com.squareup.picasso.Picasso;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

public class admin_add_ad extends AppCompatActivity {

    private EditText ETType, ETTitle, ETDate, ETTime,  ETDescription;

    private Button submitButton, chooseImage;

    private ImageView image_view;

    private static final int PICK_IMAGE_REQUEST = 1;

    private Uri ImageUri;

    private ProgressBar progressBar;

    private StorageReference StorageRef;
    private DatabaseReference DatabaseRef;

    private boolean ValidType(String type) {

        String TYPE_STRING = "SPORTS|HEALTH";

        return Pattern.compile(TYPE_STRING).matcher(type).matches();

    }

    private boolean ValidDate(String date) {

        String DATE_STRING = "^\\s*(3[01]|[12][0-9]|0?[1-9])\\/(1[012]|0?[1-9])\\/((?:19|20)\\d{2})\\s*$";

        return Pattern.compile(DATE_STRING).matcher(date).matches();
    }

    private boolean ValidTime(String time) {

        String TIME_STRING = "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";

        return Pattern.compile(TIME_STRING).matcher(time).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_ad);

        ETType = findViewById(R.id.adType);
        ETTitle = findViewById(R.id.adTitle);
        ETDate = findViewById(R.id.adDate);
        ETTime = findViewById(R.id.adTime);
        ETDescription = findViewById(R.id.adDescription);

        submitButton = findViewById(R.id.submitButton);
        chooseImage = findViewById(R.id.chooseImage);

        image_view = findViewById(R.id.image_view);

        progressBar = findViewById(R.id.progressBar);

        StorageRef = FirebaseStorage.getInstance().getReference("Advertisement").child("images");
        DatabaseRef = FirebaseDatabase.getInstance().getReference("Advertisement");

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadAd();
            }
        });

        chooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });
    }

    private void openFileChooser(){
        Intent intent  = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            ImageUri = data.getData();
            Picasso.get().load(ImageUri).into(image_view);
        }
    }

    private String getFileExtension (Uri uri){
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadAd(){

        if (TextUtils.isEmpty(ETType.getText())){
            Toast.makeText(admin_add_ad.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
            ETType.setError( "Dont Leave Me Empty :(" );

        }else if(!ValidType(ETType.getText().toString())){
            ETType.setError( "Invalid Category! \n please enter in CAPITAL LETTER" );

        }else if (TextUtils.isEmpty(ETTitle.getText())){
            Toast.makeText(admin_add_ad.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
            ETTitle.setError( "Dont Leave Me Empty :(" );

        }else if (TextUtils.isEmpty(ETDate.getText())){
            Toast.makeText(admin_add_ad.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
            ETDate.setError( "Dont Leave Me Empty :(" );

        }else if(!ValidDate(ETDate.getText().toString())){
            ETDate.setError( "Invalid Date Or Wrong Format \n Please Use DD/MM/YYYY!" );

        }else if (TextUtils.isEmpty(ETTime.getText())){
            Toast.makeText(admin_add_ad.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
            ETTime.setError( "Dont Leave Me Empty :(" );

        }else if(!ValidTime(ETTime.getText().toString())){
            ETTime.setError( "Invalid Time Or Wrong Format \n Please Specify HH:MM (24 Hour System)" );

        }else if (TextUtils.isEmpty(ETDescription.getText())){
            Toast.makeText(admin_add_ad.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
            ETDescription.setError( "Dont Leave Me Empty :(" );

        }else {

            if (ImageUri != null){

                progressBar.setVisibility(View.VISIBLE);

                StorageReference fileReference = StorageRef.child(System.currentTimeMillis()
                        + "." + getFileExtension(ImageUri));

                fileReference.putFile(ImageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>()
                {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception
                    {
                        if (!task.isSuccessful())
                        {
                            throw task.getException();
                        }
                        return fileReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task)
                    {
                        if (task.isSuccessful())
                        {
                            Uri downloadUri = task.getResult();

                            UploadAd upload = new UploadAd(ETType.getText().toString().trim(),
                                    ETTitle.getText().toString().trim(),
                                    ETDate.getText().toString().trim(),
                                    ETTime.getText().toString().trim(),
                                    ETDescription.getText().toString().trim(),
                                    downloadUri.toString());

                            DatabaseRef.push().setValue(upload);
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(admin_add_ad.this, "Upload Success", Toast.LENGTH_SHORT).show();
                        } else{

                            Toast.makeText(admin_add_ad.this, "Upload Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }else {
                Toast.makeText(this, "Image not selected", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

